
public class Name {
	public String Name;
    public String Surname;
	
    public Name (String firstName, String lastName)
    {
        Name = firstName;
        Surname = lastName;
    }

    public void setName(String firstName, String lastName)
    {
        Name = firstName;
        Surname = lastName;
    }

    public String getName()
    {
        return Name + " " + Surname;
    }

    public String toString()
    {
        return Name + " " + Surname;
    }
    
    

}
