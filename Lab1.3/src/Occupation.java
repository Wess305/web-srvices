
public class Occupation {

	 public String Occupation;

	    public Occupation(String occupation)
	    {
	        this.Occupation = occupation;
	    }

	    public void setOccupation(String occupation)
	    {
	        this.Occupation = occupation;
	    }

	    public String getOccupation()
	    {
	        return this.Occupation;
	    }

	    public String toString()
	    {
	        return this.Occupation;
	    }
}
