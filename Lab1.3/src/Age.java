
public class Age {
	public int Age;

    public Age(int age)
    {
        this.Age = age;
    }

    public void setAge(int age)
    {
        this.Age = age;
    }

    public int getAge()
    {
        return this.Age;
    }

    public String toString()
    {
        return ""+this.Age;
    }

}
