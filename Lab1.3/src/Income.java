
public class Income {

	 private double SalaryPerAnnum;

	    public Income(double salaryPerAnnum)
	    {
	        this.SalaryPerAnnum = salaryPerAnnum;
	    }

	    public void setSalary(double salaryPerAnnum)
	    {
	        this.SalaryPerAnnum = salaryPerAnnum;
	    }

	    public double getSalary()
	    {
	        return this.SalaryPerAnnum;
	    }

	    public String toString()
	    {
	        String result = "$";
	        result += this.SalaryPerAnnum;
	        return result;
	    }
}
