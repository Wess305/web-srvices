import java.util.*;
public class Main {

	public static void main(String[] args) {
		
        int[] store = new int[51];
        System.out.println("Enter any integers between"+
        " 0 and 50, and if you are done then enter any"+
        " integer out of the range");
        System.out.println("Enter Integer: ");
        Scanner scan = new Scanner(System.in);
        int integer = scan.nextInt();
      
        while(integer >= 0 && integer <= 51)
        {
            
            store[integer] = store[integer] + 1;
            System.out.println("Enter Integer: ");
            integer = scan.nextInt();
        }
     
        System.out.println("printing number of times"+ 
            " each integer is entered");
        for (int i=0; i < 51; i++) 
        {  
            if (store[i] > 0)
                System.out.println(i + ": " + store[i]);
        }

	}

}
