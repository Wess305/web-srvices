
public class Receptionist extends Administrator {
	public Receptionist(String name, String address, String phone, 
	    	int employee_number)
	    {
	        super(name, address, phone, employee_number);
	    }

	    
	    public void answerPhone()
	    {
	        System.out.println("Hello receptionist here...\nHow can I help you");
	    }

}
