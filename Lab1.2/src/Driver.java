
public class Driver {
	 public static void main (String[] args)
	    {   
	        Doctor doctor = new Doctor ("Alex", "Kiev", 
	        	"8564257791", 11112, 55, "11 may");
	        Surgeon surgeon = new Surgeon("Mune", "harare",
	        	"2354761578", 11125, 55, "27 may");
	        Administrator admin = new Administrator("Muna" ,
	        	"tokyo", "9554425731", 11119);
	        doctor.print();
	        System.out.println("----------");
	        surgeon.print();
	        System.out.println("----------");
	        admin.orderEquipment();
	        System.out.println("----------");
	        surgeon.operate();
	        System.out.println("\n\n");
	    }

}
